import subprocess
def ping(ip):
    command = ['ping', '-n', '2', ip]
    return subprocess.run(command, capture_output=True, text=True, shell=True)
ip = input();
response = ping(ip)
print('return code: ', response.returncode)
print('response: ', response.stdout)
print('errors: ', response.stderr)
